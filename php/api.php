<?php

class VIP
{	
	/**
	 * API Key
	 * @var string
	 */
	private $key;

	/**
	 * API Secret
	 * @var string
	 */
	private $secret;

	/**
	 * Set API Key and API Secret
	 * @param  array  $config
	 */
	public function setupAPI( $config = array() )
	{
		if ( ! empty( $config['key'] ) && $config['secret'] ) 
		{
			$this->key      = $config['key'];
			$this->secret   = $config['secret'];
		}
	}

	/**
	 * Get current price
	 * @param  string $currency
	 * @return array          
	 */
	public function getTicker( $currency = 'btc_idr' )
	{
		
		return $this->fetch('https://vip.bitcoin.co.id/api/' . $currency . '/ticker');
	}

	/**
	 * Get current trade sell and buy
	 * @param  string $currency
	 * @return array          
	 */
	public function getTrade( $currency = 'btc_idr' )
	{
		
		return $this->fetch('https://vip.bitcoin.co.id/api/' . $currency . '/trades');
	}

	/**
	 * Get current depth sell and buy
	 * @param  string $currency
	 * @return [type]          
	 */
	public function getDepth( $currency = 'btc_idr' )
	{
		return $this->fetch('https://vip.bitcoin.co.id/api/' . $currency . '/depth');
	}

	/**
	 * Fetching data based on given URL
	 * @param  string $url
	 * @return array     
	 */
	public function fetch( $url = '' )
	{
		$result = file_get_contents( $url );

		if ( ! empty( $result ) && $result ) 
			return json_decode($result, true);
	}

	/**
	 * Return general info including current balance
	 * @return array
	 */
	public function getInfo()
	{
		return $this->request( 'getInfo' );
	}

	/**
	 * Use to make a new order
	 * @param  array  $args
	 * @return array      
	 */
	public function trade( $args )
	{
		$currency = explode( '_', $args['pair'] );

		if ( $args['type'] === 'buy' )
		{
			$args[$currency[1]] = $args['amount'];
		}
		else if ( $args['type'] === 'sell' )
		{
			$args[$currency[0]] = $args['amount'];
		}

		unset( $args['amount'] );

		return $this->request( 'trade', $args );
	}

	/**
	 * Return information about buy and sell transaction history
	 * @param  array  $args
	 * @return array      
	 */
	public function tradeHistory( $args )
	{
		return $this->request( 'tradeHistory', $args );
	}

	/**
	 * Return information about existing open order
	 * @param  array $args
	 * @return array      
	 */
	public function openOrders( $args )
	{
		return $this->request( 'openOrders', $args );
	}

	/**
	 * Use to cancel specific open order
	 * @param  array $args
	 * @return array      
	 */
	public function cancelOrder( $args )
	{
		return $this->request( 'cancelOrder', $args );
	}

	/**
	 * Make API request
	 * @param  string $method
	 * @param  array  $args  
	 * @return array        
	 */
	public function request( $method, $args = array() )
	{
		if ( ! isset( $this->key ) || empty( $this->key ) )
			throw new Exception( 'Missing API Key!', 1 );

		if ( ! isset( $this->secret ) || empty( $this->secret ) )
			throw new Exception( 'Missing API Secret!', 1 );

		$args['method'] = $method;
		$args['nonce']  = (int) time();

		// generate the POST data string
		$post_data  = http_build_query( $args, '', '&' );
		$sign       = hash_hmac( 'sha512', $post_data, $this->secret );

		// generate the extra headers
		$headers = array(
			'Sign: '. $sign,
			'Key: '. $this->key
		);

		// our curl handle (initialize if required)
		static $ch = null;
		if ( is_null( $ch ) )
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; BITCOINCOID PHP client; '.php_uname('s').'; PHP/'.phpversion().')');
			curl_setopt($ch, CURLOPT_URL, 'https://vip.bitcoin.co.id/tapi/');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		}

		// run the query
		$result = curl_exec($ch);

		if ( $result === false )
			throw new Exception( 'Could not get reply:' . curl_error( $ch ) );

		$result = json_decode($result, true);

		if ( empty( $result ) )
			throw new Exception( 'Invalid data received, please make sure connection is working and requested API exists: '. $result );

		curl_close( $ch );

		$ch = null;

		return $result;
	}
}

// $log = new VIP();

// var_dump($log->getTicker('str_idr'));

// $log->setupAPI(
// 	array(
// 		'key'       => 'API_KEY',
// 		'secret'    => 'API_SECRET'
// 	)
// );

// $log->getInfo();

// $log->tradeHistory(
// 	array(
// 		'pair' => 'doge_btc',
// 		'count' => 10
// 	)
// );

// $log->trade(
// 	array(
// 		'pair' => 'doge_btc',
// 		'type' => 'buy',
// 		'price' => 0.00000011, // price of doge
// 		'amount' => 0.0001, // price * amount of doge coin
// 	)
// );

// $log->trade(
// 	array(
// 		'pair' => 'doge_btc',
// 		'type' => 'sell',
// 		'price' => 0.00000102, // price of doge
// 		'amount' => 2, // amount of doge coin
// 	)
// );