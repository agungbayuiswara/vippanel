(function($){

    window.vip = window.vip || {};

    window.vip.view = 
    {
        selector : null,
        pushLimit : 20,
        init : function()
        {
            this.selector = vip.selector;
            this.spotTable();
            this.tradingButton();
            this.tradingAnalytic();
            this.injectStyle();
        },
        createSpotTd : function(selector, text)
        {
            return "<td class='" + selector + "'><strong></strong><br/><span class='text-muted'>" + text + "</span></td>";
        },
        createTradingButton: function(selector, text, icon)
        {
            var icon_html = ( icon === undefined ) ? "" : "<i class='fa " + icon + "'></i>";
            return "<button type='button' class='btn " + selector + "' data-toggle='dropdown'>" + icon_html + " " + text +"</button>";
        },
        spotTable : function()
        {
            var base = this;

            base.selector.spotStat.find('.market-price-box').before(
                base.createSpotTd('trading percent-diff', 'Different') +
                base.createSpotTd('trading cut-loss', 'Cut Loss')
            );

            base.selector.spotStat.find('.market-price-box').after(
                base.createSpotTd('no-trading one-percent', '1% Gain') +
                base.createSpotTd('no-trading two-percent', '2% Gain') +
                base.createSpotTd('no-trading three-percent', '3% Gain') +
                base.createSpotTd('trading buy', 'Trade Buy') +
                base.createSpotTd('trading one-percent', '1% Gain') +
                base.createSpotTd('trading two-percent', '2% Gain') +
                base.createSpotTd('trading three-percent', '3% Gain')
            );
        },
        tradingButton : function()
        {
            var base = this;

            base.selector.headerRight.prepend(
                "<div class='pull-right buy-button-wrap'>"+
                    base.createTradingButton('buy-button buy-one', 'Buy 1%') +
                    base.createTradingButton('buy-button buy-two', 'Buy 2%') +
                    base.createTradingButton('buy-button buy-three', 'Buy 3%') +
                    base.createTradingButton('buy-button buy-last-price', 'Buy Last Price') +
                    base.createTradingButton('buy-button buy-panic', 'Panic Buy') +
                "</div>" +
                "<div class='pull-right sell-button-wrap'>" +
                    base.createTradingButton('sell-button sell-cut-loss', 'Sell Cut Loss') +
                    base.createTradingButton('sell-button sell-three', 'Sell 3%') +
                    base.createTradingButton('sell-button sell-six', 'Sell 6%') +
                    base.createTradingButton('sell-button sell-last-price', 'Sell Last Price') +
                    base.createTradingButton('sell-button sell-panic', 'Panic Sell') +
                "</div>" +
                "<div class='pull-right trading-button-wrap'>"+
                    base.createTradingButton('trading-button', 'Trading Form') +
                "</div>"
            );
        },
        createTradingTable: function(title, selector, height, theads)
        {
            var thead = '';

            $(theads).each(function(index, data){
                thead += "<th style='width: " + data.width + ";'>" + data.title + "</th>";
            });

            return "<div class='" + selector + "'>" +
                        "<h5 class='mb5'><strong>" + title + "</strong></h5>" +
                        "<div class='table-responsive table-border-selector' style=' height: " + height + "px; overflow: auto;'>" +
                            "<table class='table table-primary mb30 table-bordered'>" +
                                "<thead>" +
                                    "<tr>" +
                                        thead +
                                    "</tr>" +
                                "</thead>" +
                                "<tbody>" +
                                "</tbody>" +
                            "</table>" +
                        "</div>" +
                    "</div>";
        },
        tradingOtherMarket: function()
        {
            return this.createTradingTable('Other Market', 'other-market', 135, [
                { title : 'Market', width: 'auto' },
                { title : 'Price', width: 'auto' },
                { title : 'Different', width: 'auto' }
            ]);
        },
        tradingActivityVolume: function()
        {
            return this.createTradingTable('Trading Activity Volume', 'trading-volume', 170, [
                { title : 'Trend', width: 'auto' },
                { title : 'Buy %', width: '22%' },
                { title : 'Sell %', width: '22%' },
                { title : 'Velocity', width: '22%' }
            ]);
        },
        tradingActivityStatus: function()
        {
            return this.createTradingTable('Trading Activity Status', 'trading-status', 170, [
                { title : 'Trend', width: 'auto' },
                { title : 'N. Buy', width: '22%' },
                { title : 'N. Sell', width: '22%' },
                { title : 'Price Diff', width: '22%' }
            ]);
        },
        marketStatistic: function()
        {
            return this.createTradingTable('Buy & Sell Market Statistic', 'market-statistic', 170, [
                { title : 'Trend', width: 'auto' },
                { title : 'Buy %', width: '22%' },
                { title : 'Sell %', width: '22%' },
                { title : 'Gap', width: '22%' }
            ]);
        },
        priceStatistic: function()
        {
            var statistic = [];
            for(var i = 1; i <= vip.constant.PRICE_STATISTIC; i++) {
                statistic.push({
                    title : i * vip.constant.PRICE_DELAY + "s",
                    width: 100 / vip.constant.PRICE_STATISTIC
                });
            }

            return this.createTradingTable('Price Statistic', 'price-statistic', 170, statistic);
        },
        priceChangeStatistic: function()
        {
            return this.createTradingTable('Price Change', 'price-change', 170, [
                { title : 'Change', width: '33%' },
                { title : 'Percentage', width: '33%' },
                { title : 'Time Gap', width: '33%' }
            ]);
        },
        tradingAnalytic: function()
        {
            var base = this;

            base.selector.mainWrapper.after(
                "<div class='trading-analytics'>" +
                    "<h3>Trade Analytics</h3>" +
                    base.tradingOtherMarket() +
                    base.priceChangeStatistic() +
                    base.priceStatistic() +
                    base.tradingActivityVolume() +
                    base.tradingActivityStatus() +
                    base.marketStatistic() +
                "</div>"
            );
        },
        marketPriceBox : function(one, two, three)
        {
            var base = this;

            base.selector.spotStat.find('.no-trading.one-percent > strong').text(vip.helper.formatPrice(one));
            base.selector.spotStat.find('.no-trading.two-percent > strong').text(vip.helper.formatPrice(two));
            base.selector.spotStat.find('.no-trading.three-percent > strong').text(vip.helper.formatPrice(three));
        },
        tradePriceBox : function(buy, one, two, three, cutloss)
        {
            var base = this;

            base.selector.spotStat.find('.trading.cut-loss > strong').text(vip.helper.formatPrice(cutloss));
            base.selector.spotStat.find('.trading.buy > strong').text(vip.helper.formatPrice(buy));
            base.selector.spotStat.find('.trading.one-percent > strong').text(vip.helper.formatPrice(one));
            base.selector.spotStat.find('.trading.two-percent > strong').text(vip.helper.formatPrice(two));
            base.selector.spotStat.find('.trading.three-percent > strong').text(vip.helper.formatPrice(three));
        },
        formatPercentage: function(price)
        {
            return price + '%';
        },
        formatPercentageDiff: function(value)
        {
            if(value > 0)
            {
                return "<span class='up'>" + value + '%' + "</span>";
            } else {
                return "<span class='down'>" + value + '%' + "</span>";
            }
        },
        formatPriceDiff: function(price)
        {
            if(price > 0)
            {
                return "<span class='up'>" + vip.helper.formatPrice(price) + "</span>";
            } else {
                return "<span class='down'>" + vip.helper.formatPrice(price) + "</span>";
            }
        },
        formatBuySell: function(value)
        {
            var state = value.toLowerCase();

            if(state === 'buy' || state === 'beli')
            {
                return "<span class='up'>" + value + "</span>";
            } else {
                return "<span class='down'>" + value + "</span>";
            }
        },
        formatTableContent: function(selector, index, value)
        {
            if(selector === '.other-market')
            {
                if(index === 1) return vip.helper.formatPrice(value);
                if(index === 2) return this.formatPercentageDiff(value);
            }

            if(selector === '.trading-volume')
            {
                if(index === 0) return this.formatBuySell(value);
                if(index === 1) return this.formatPercentage(value);
                if(index === 2) return this.formatPercentage(value);
            }

            if(selector === '.trading-status')
            {
                if(index === 0) return this.formatBuySell(value);
                if(index === 1) return this.formatPercentage(value);
                if(index === 2) return this.formatPercentage(value);
                if(index === 3) return vip.helper.formatPrice(value);
            }

            if(selector === '.market-statistic')
            {
                if(index === 0) return this.formatBuySell(value);
                if(index === 1) return this.formatPercentage(value);
                if(index === 2) return this.formatPercentage(value);
                if(index === 3) return vip.helper.formatPrice(value);
            }

            if(selector === '.price-change')
            {
                if(index === 0) return this.formatPriceDiff(value);
                if(index === 1) return this.formatPercentageDiff(value);
                if(index === 2) return value + " second";
            }

            if(selector === '.price-statistic')
            {
                return this.formatPercentageDiff(value);
            }

            return value;
        },
        createTableContent: function(selector, data)
        {
            var base = this;
            var html = '';

            $(data).each(function(index, row)
            {
                html += '<tr>';

                $(row).each(function(index, value)
                {
                    value = base.formatTableContent(selector, index, value);
                    html +=  "<td>" + value + "</td>";
                });

                html += '</tr>';
            });

            return html;
        },
        pushToTable: function(selector, data, status)
        {
            var base = this;
            var $tbody = $(selector).find('tbody');
            var content = base.createTableContent(selector, data);

            if(status === vip.constant.PUSH)
            {
                if( ( $tbody.find('tr').length + data.length ) > base.pushLimit)
                {
                    var numberRemove = ( $tbody.find('tr').length + data.length ) % base.pushLimit;
                    for(var i = 0; i < numberRemove; i++) {
                        $tbody.find('tr').last().remove();
                    }
                }
                $tbody.prepend(content);
            } else if(status === vip.constant.REPLACE) {
                $tbody.html(content);
            }

            $(window).trigger(selector.replaceAll('.', '').trim());
        },
        injectStyle: function()
        {
            $.get(styleUrl, function(data) {
                $("head").append("<style type='text/css'>" + data + "</style>");
            });
        }
    };

    vip.view.init();

})(jQuery);