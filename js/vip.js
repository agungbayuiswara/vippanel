/**
 * Replace all functionality
 *
 * @param search
 * @param replacement
 * @returns {string}
 */
String.prototype.replaceAll = function(search, replacement)
{
    var target = this;
    return target.split(search).join(replacement);
};

(function($){

    window.vip = window.vip || {};

    window.vip.constant = {
        SPACE           : ' ',
        REPLACE         : 'replace',
        PUSH            : 'push',
        CURRENCY        : ['btc', 'bch', 'btg', 'eth', 'etc', 'ltc', 'nxt', 'waves', 'str', 'xrp', 'xzc'],
        RECORD_INTERVAL : 30 * 1000,
        SIGNAL_INTERVAL : 10 * 1000,
        DEVMODE         : false,

        MARKET_IDR      : 'IDR',
        MARKET_BTC      : 'BTC',
        MARKET_TYPE     : null,
        MARKET_COIN     : null,

        TRADE_BUY       : 'buy',
        TRADE_SELL      : 'sell',
        TRADE_FEE       : 0.6,

        PRICE_STATISTIC : 6,
        PRICE_DELAY     : 10,
        PRICE_ROW_CHECK : 2,

        CUT_LOSS_PERCENTAGE     : -1.00,
        MARKET_COST             : 0.6,
        PRICE_DROP_TOLERANCE    : 0.5,

        COIN_DECIMAL    : {
            BTS : 8,
            DASH : 6,
            DOGE : 8,
            ETH : 5,
            LTC : 6,
            NXT : 8,
            XLM : 8,
            XEM : 8,
            XRP : 8
        },

        setup            : function()
        {
            var code = $('.spot-title-container h5').text();
            vip.constant.MARKET_TYPE = code.substr(code.length - 3);
            vip.constant.MARKET_COIN = code.substr(0, code.indexOf('/'));

            if(vip.constant.MARKET_TYPE === vip.constant.MARKET_BTC)
            {
                vip.constant.MARKET_COST = 0;
                vip.constant.CUT_LOSS_PERCENTAGE = -0.6;
            }
        },
    };

    vip.constant.setup();


    window.vip.selector = {
        tradeCurrency   : window.traded_currency,
        mainWrapper     : $('.mainpanel'),
        body            : $('body'),
        spotStat        : $('.spot-stat-container'),
        myLastTrades    : $('#my_last_trades'),
        headerRight     : $('.headerwrapper .header-right'),
        otherMarket     : $('.pageheader.marquee'),
        btnTradeForm    : $('.contentpanel > div:nth-child(3)'),
        cancelOrder     : $('#pending_orders_container'),
        buyForm         : $('#wizard-beli'),
        sellForm        : $('#wizard-jual'),
        tradingActivity : $('#last_trades'),
        sellMarket      : $('#sell_orders'),
        buyMarket       : $('#buy_orders'),
        markets         : $('#markets')
    };

    vip.selector.lastPrice  = vip.selector.spotStat.find('.market-price-box > strong');

    window.vip.helper =
    {
        getCurrentTime: function()
        {
            var d = new Date();
            return d.getTime();
        },
        strPriceToInt: function(price)
        {
            if(typeof price === 'undefined') return;

            if(vip.constant.MARKET_TYPE === vip.constant.MARKET_IDR)
            {
                return parseInt(price.replaceAll('.', '').replaceAll(',', ''));
            } else {
                if(typeof price === 'string') {
                    return parseFloat(price.replace(',', '.'));
                } else {
                    return price;
                }
            }
        },
        selectorPriceToInt: function(selector)
        {
            var price = $(selector).text();

            if(price === '') return false;
            return this.strPriceToInt(price);
        },
        getLastPrice: function()
        {
            return this.selectorPriceToInt(vip.selector.lastPrice);
        },
        formatPrice: function(price)
        {
            if(vip.constant.MARKET_TYPE === vip.constant.MARKET_IDR) {
                return $.number(price.toFixed(), 0, ',', '.');
            } else {
                return parseFloat(price).toFixed(vip.constant.COIN_DECIMAL[vip.constant.MARKET_COIN]);
            }
        },
        calculateMinutes: function(begin, end)
        {
            var minutes = ( end - begin ) / ( 1000 * 60 );
            return minutes.toFixed(2);
        },
        getCurrentTimeString: function()
        {
            return new Date().toLocaleString();
        }
    };

})(jQuery);