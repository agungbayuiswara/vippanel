(function($){

    "use strict";

    window.vip = window.vip || {};

    window.vip.data = 
    {
        priceRecordPrefix   : 'price_',
        tradeStatus         : 0,
        currentMarketPrice  : 0,
        lastTradePrice      : 0,
        lastSellMarketPrice : 0,
        lastSellMarketIDR   : 0,
        lastBuyMarketPrice  : 0,
        lastBuyMarketIDR    : 0,
        tradeBuyVolume      : 0,
        tradeSellVolume     : 0,
        totalBuyMarket      : 0,
        totalSellMarket     : 0,
        lastPrice           : 0,
        lastPriceChange     : 0,

        init : function()
        {
            this.checkTradeStatus();
            this.otherMarketPrice();
            this.tradingActivity();
            this.marketActivity();
            this.priceStatistic();
            this.priceChange();

            // this.priceRecord();
        },
        checkTradeStatus : function()
        {
            // checking the last trade status
            
            var base = this;

            vip.selector.myLastTrades.on('DOMSubtreeModified', function()
            {
                var element, status;

                element = $(this).find('tbody > tr').get(0);

                if ( 'undefined' === typeof element ) return false;

                    status  = $(element).find('td').get(1);
                    status  = $(status).text().trim();

                if ( status )
                {
                    if ( status === 'Beli' ) 
                    {
                        vip.selector.body.removeClass('status-sell').addClass('status-buy');
                        base.tradeStatus = vip.helper.strPriceToInt($(element).attr('price'));
                        base.updateTradePriceBox();
                    } else {
                        vip.selector.body.removeClass('status-buy').addClass('status-sell');
                        base.updateMarketPriceBox();
                    }
                } else {
                    vip.selector.body.removeClass('status-buy').addClass('status-sell');
                    base.updateMarketPriceBox();
                }
            });
        },
        updateMarketPriceBox : function()
        {
            // update market gain price box (last trade = sell)

            var base = this;

            vip.selector.lastPrice.on('DOMSubtreeModified', function()
            {
                var marketPrice     = vip.helper.strPriceToInt($(this).text()),
                    onePercent      = 0,
                    twoPercent      = 0,
                    threePercent    = 0;

                if ( marketPrice > 0 )
                {
                    if ( base.currentMarketPrice !== marketPrice )
                    {
                        base.currentMarketPrice = marketPrice;

                        onePercent      = base.calculatePercentage(1, marketPrice, '+');
                        twoPercent      = base.calculatePercentage(2, marketPrice, '+');
                        threePercent    = base.calculatePercentage(3, marketPrice, '+');

                        vip.view.marketPriceBox(onePercent, twoPercent, threePercent);
                    }
                }
            });
        },
        updateTradePriceBox : function()
        {
            // update trade gain price box (last trade = buy)

            var base            = this,
                cutLoss         = base.calculatePercentage(1, base.tradeStatus, '-'),
                onePercent      = base.calculatePercentage(1, base.tradeStatus, '+'),
                twoPercent      = base.calculatePercentage(2, base.tradeStatus, '+'),
                threePercent    = base.calculatePercentage(3, base.tradeStatus, '+'),
                tradeBuy        = base.tradeStatus;

            vip.view.tradePriceBox(tradeBuy, onePercent, twoPercent, threePercent, cutLoss);

            vip.selector.lastPrice.on('DOMSubtreeModified', function()
            {
                var marketPrice = vip.helper.selectorPriceToInt($(this)),
                    diffPercent = 0,
                    sign        = '';

                if ( marketPrice && marketPrice > 0 )
                {
                    if ( base.currentMarketPrice !== marketPrice )
                    {
                        base.currentMarketPrice = marketPrice;

                        diffPercent = ( base.currentMarketPrice - base.tradeStatus ) / base.tradeStatus * 100;

                        if ( diffPercent > 0 ) 
                        {
                            sign = '+';
                            vip.selector.spotStat.find('.trading.percent-diff').removeClass('warning').addClass('gain');
                        } else if (diffPercent < 0) {
                            vip.selector.spotStat.find('.trading.percent-diff').removeClass('gain').addClass('warning');
                        }

                        vip.selector.spotStat.find('.trading.percent-diff > strong').text( sign + diffPercent.toFixed(2) + '%');
                    }
                }
            });
        },
        calculatePercentage : function(percent, price, operator)
        {
            percent = ( price * percent ) / 100;

            if ( operator === '+' )
            {
                price = price + percent;
            } else {
                price = price - percent;
            }

            return price;
        },
        otherMarketPrice : function()
        {
            var base = this,
                item = [];

            vip.selector.otherMarket.on('DOMSubtreeModified', function()
            {
                var prices = 0, index = 0, percent;

                $(this).find('span').each(function()
                {
                    var element, regExp, market, matches, price, percent, text;

                    element = $(this);

                    if(vip.constant.MARKET_TYPE === vip.constant.MARKET_IDR) {
                        regExp  = /\(([^)]+)\)/;
                        market  = element.find('b').text().replaceAll(':', '');
                        matches = regExp.exec(element.text());
                        price   = vip.helper.strPriceToInt(matches[1]);
                    } else {
                        market  = element.find('b').text().replaceAll(':', '');
                        text    = element.text().substr(element.text().indexOf(':') + 2);
                        price   = vip.helper.strPriceToInt(text);
                    }

                    percent = ( ( price - base.currentMarketPrice) / base.currentMarketPrice ) * 100;
                    prices = prices + price;
                    index++;

                    item[market] = [market, price, percent.toFixed(2)];
                });

                prices  = prices / index;
                percent = ((prices - base.currentMarketPrice) / base.currentMarketPrice) * 100;
            });

            setInterval(function()
            {
                var data = [];

                for ( var key in item )
                {
                    data.push(item[key]);
                }

                vip.view.pushToTable('.other-market', data, vip.constant.REPLACE);
            }, 2000);

        },
        tradingActivity : function()
        {
            var base        = this,
                numberBuy   = 0,
                numberSell  = 0,
                diffPrice   = 0,
                totalBuy    = 0,
                totalSell   = 0;

            vip.selector.tradingActivity.on('DOMSubtreeModified', function()
            {
                var element       = $(this),
                    newTrade      = $(element.find('tbody tr').get(0)),
                    newTradePrice = vip.helper.strPriceToInt(newTrade.attr('price')),
                    maxIndex      = 10;

                if ( newTrade.length > 0 )
                {
                    if ( newTradePrice !== base.lastTradePrice )
                    {
                        diffPrice           = newTradePrice - base.lastTradePrice;
                        base.lastTradePrice = newTradePrice;
                        numberBuy   = 0;
                        numberSell  = 0;
                        totalBuy    = 0;
                        totalSell   = 0;

                        element.find('tbody tr').each(function()
                        {
                            var element = $(this);

                            if ( element.index() < maxIndex )
                            {
                                var price = vip.helper.strPriceToInt(element.attr('price')),
                                    type  = $(element.find('td').get(1)).text();

                                if ( type === 'Jual' )
                                {
                                    totalSell = totalSell + price;
                                    numberSell++;
                                } else {
                                    totalBuy = totalBuy + price;
                                    numberBuy++;
                                }
                            }
                        });
                    }
                }
            });

            setInterval(function()
            {
                var trend, trendPercent, totalTrade;

                if ( numberBuy > numberSell )
                {
                    trend = 'Buy';
                } else {
                    trend = 'Sell';
                }

                if ( totalBuy > totalSell )
                {
                    trendPercent = 'Buy';
                } else {
                    trendPercent = 'Sell';
                }

                totalTrade = totalBuy + totalSell;
                totalBuy  = (totalBuy / totalTrade) * 100;
                totalSell = (totalSell / totalTrade) * 100;

                vip.view.pushToTable('.trading-volume', [[trendPercent, totalBuy.toFixed(), totalSell.toFixed(), base.marketVelocity()]], vip.constant.PUSH);
                vip.view.pushToTable('.trading-status', [[trend, numberBuy*10, numberSell*10, Math.abs(diffPrice)]], vip.constant.PUSH);

            }, 2000);
        },
        marketVelocity : function()
        {
            var base = this,
                velocity      = 0,
                lastMinute    = 0,
                currentMinute = 0;

            vip.selector.tradingActivity.find('tbody tr').each(function()
            {
                currentMinute = $(this).find('td').first().text().split(':')[1];

                if ( currentMinute !== lastMinute )
                {
                    velocity++;
                    lastMinute = currentMinute;
                }
            });

            velocity = 100 / velocity;

            return velocity.toFixed();
        },
        priceChange: function()
        {
            var base = this;
            var price = 0,
                priceChange = 0,
                timeDiff = 0,
                changePercentage = 0;

            vip.selector.lastPrice.on('DOMSubtreeModified', function()
            {
                if(price  = vip.helper.getLastPrice())
                {
                    // Initialize Price
                    if(base.lastPrice === 0)
                    {
                        base.lastPrice = price;
                        base.lastPriceChange = vip.helper.getCurrentTime();
                    }

                    if(base.lastPrice !== price)
                    {
                        priceChange      = price - base.lastPrice;
                        timeDiff         =  ( vip.helper.getCurrentTime() - base.lastPriceChange ) / 1000;
                        changePercentage = priceChange / price * 100;

                        vip.view.pushToTable('.price-change', [[priceChange, changePercentage.toFixed(4), timeDiff.toFixed(1)]], vip.constant.PUSH);

                        base.lastPrice = price;
                        base.lastPriceChange = vip.helper.getCurrentTime();
                    }
                }

            });
        },
        marketActivity : function()
        {
            var base = this,
                trend, totalMarket, gapMarket, buyPercent, sellPercent;

            vip.selector.sellMarket.on('DOMSubtreeModified', function()
            {
                base.sellMarketActivity($(this));
            });

            vip.selector.buyMarket.on('DOMSubtreeModified', function()
            {
                base.buyMarketActivity($(this));
            });

            setInterval(function()
            {
                totalMarket         = base.totalSellMarket + base.totalBuyMarket;
                gapMarket           = Math.abs( base.lastBuyMarketPrice - base.lastSellMarketPrice );
                buyPercent          = ( base.totalBuyMarket / totalMarket ) * 100;
                sellPercent         = ( base.totalSellMarket / totalMarket ) * 100;

                if ( base.totalBuyMarket > base.totalSellMarket )
                {
                    trend = 'Buy';
                } else {
                    trend = 'Sell';
                }

                vip.view.pushToTable('.market-statistic', [[trend, buyPercent.toFixed(), sellPercent.toFixed(), gapMarket]], vip.constant.PUSH);
            }, 2000);
        },
        sellMarketActivity : function(element)
        {
            var base        = this,
                newMarket   = $(element.find('tbody tr').get(0)),
                newPrice    = vip.helper.strPriceToInt(newMarket.attr('price')),
                newIDR      = vip.helper.selectorPriceToInt(newMarket.find('td').last()),
                maxIndex    = 10;

            if ( newMarket.length > 0 )
            {
                if ( newPrice !== base.lastSellMarketPrice || newIDR !== base.lastSellMarketIDR )
                {
                    base.lastSellMarketPrice = newPrice;
                    base.lastSellMarketIDR   = newIDR;
                    base.totalSellMarket     = 0;

                    element.find('tbody tr').each(function()
                    {
                        var element = $(this);

                        if ( element.index() <= maxIndex )
                        {
                            var price = vip.helper.strPriceToInt(element.attr('price')),
                                idr   = vip.helper.selectorPriceToInt(element.find('td').last());

                            base.totalSellMarket = base.totalSellMarket + ( price * idr );
                        }
                    });
                }
            }
        },
        buyMarketActivity : function(element)
        {
            var base        = this,
                newMarket   = $(element.find('tbody tr').get(0)),
                newPrice    = vip.helper.strPriceToInt(newMarket.attr('price')),
                newIDR      = vip.helper.selectorPriceToInt(newMarket.find('td').last()),
                maxIndex    = 10;

            if ( newMarket.length > 0 )
            {
                if ( newPrice !== base.lastBuyMarketPrice || newIDR !== base.lastBuyMarketIDR )
                {
                    base.lastBuyMarketPrice = newPrice;
                    base.lastBuyMarketIDR   = newIDR;
                    base.totalBuyMarket     = 0;

                    element.find('tbody tr').each(function()
                    {
                        var element = $(this);

                        if ( element.index() <= maxIndex )
                        {
                            var price = vip.helper.strPriceToInt(element.attr('price')),
                                idr   = vip.helper.selectorPriceToInt(element.find('td').last());

                            base.totalBuyMarket = base.totalBuyMarket + ( price * idr );
                        }
                    });
                }
            }
        },
        priceStatistic : function()
        {
            var base = this,
                data = [],
                dataDiff = [],
                maxIndex = vip.constant.PRICE_STATISTIC + 1;

            setInterval(function()
            {
                dataDiff = [];

                if ( data.length < maxIndex )
                {
                    data.unshift(base.currentMarketPrice);
                } else {
                    data.splice(-1,1);
                    data.unshift(base.currentMarketPrice);
                }

                for ( var key in data )
                {
                    if ( key > 0 )
                    {
                        var a = (data[0] - data[key]) / data[key] * 100;
                        dataDiff.push( a.toFixed(3) );
                    }
                }

                vip.view.pushToTable('.price-statistic', [dataDiff], vip.constant.PUSH);

            }, vip.constant.PRICE_DELAY * 1000);
        },
        priceRecord : function()
        {
            var base = this;

            setInterval(function()
            {
                $(vip.constant.CURRENCY).each(function(index, item)
                {
                    base.doPriceRecord(item);
                });
            }, vip.constant.RECORD_INTERVAL);
        },
        doPriceRecord : function(currency)
        {
            var base  = this,
                price = vip.helper.selectorPriceToInt(vip.selector.markets.find('tbody span[class="price_' + currency + 'idr_val"]')),
                prices;

            if ( price > 0 )
            {
                prices = base.getPriceRecord(currency);
                prices.push( [$.now(), price] );

                localStorage.setItem(base.getPriceRecordKey(currency), JSON.stringify(prices));
            }
        },
        getPriceRecord : function(currency)
        {
            var base   = this,
                prices = localStorage[base.getPriceRecordKey(currency)];

            if ( typeof prices !== 'undefined' )
            {
                return JSON.parse(prices);
            }

            return [];
        },
        getPriceRecordKey : function(currency)
        {
            var base = this;

            return base.priceRecordPrefix + currency;
        },
        resetPriceRecord : function(currency)
        {
            var base = this;

            if ( currency === 'all' )
            {
                $(vip.constant.CURRENCY).each(function(index, item)
                {
                    localStorage.removeItem(base.getPriceRecordKey(item));
                });
            } else {
                localStorage.removeItem(base.getPriceRecordKey(currency));
            }
        }
    };

    vip.data.init();

})(jQuery);