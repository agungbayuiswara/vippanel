// ==UserScript==
// @name         vippannel
// @namespace    http://tampermonkey.net/
// @version      1.0
// @author       You
// @match        https://vip.bitcoin.co.id/market/*
// @grant        none
// ==/UserScript==

(function($) 
{
    "use strict";

    var url = "https://localhost.com/vip/";
    window.styleUrl = url + 'css/vip.css';

    $("body").append("<script type='text/javascript' src='" + url + "js/vip.js'></script>");
    $("body").append("<script type='text/javascript' src='" + url + "js/vip.view.js'></script>");
    $("body").append("<script type='text/javascript' src='" + url + "js/vip.data.js'></script>");
    $("body").append("<script type='text/javascript' src='" + url + "js/vip.trade.js'></script>");
})(jQuery);