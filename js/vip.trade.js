(function($){

    window.vip = window.vip || {};

    window.vip.trade =
    {
         signalRecordPrefix         : 'signal_',
         tradeRecordPrefix          : 'trade_',

         init : function()
         {
             this.tradeForm();
             this.tradeBuy();
             this.tradeSell();
             // this.tradeSignal();
             this.tradeRecord();
         },
         tradeForm : function()
         {
             var base = this;

             vip.selector.body.on('click', '.trading-button', function(e)
             {
                 e.preventDefault();

                 if ( $(this).hasClass('active') )
                 {
                     $(this).removeClass('active');
                 } else {
                     $(this).addClass('active');
                 }

                 if ( vip.selector.btnTradeForm.hasClass('show') )
                 {
                     vip.selector.btnTradeForm.removeClass('show');
                 } else {
                    vip.selector.btnTradeForm.addClass('show');
                 }
             });
         },
         tradeSell : function()
         {
             var base = this;

             vip.selector.body.on('click', '.sell-button', function(e)
             {
                 e.preventDefault();

                 var button      = $(this),
                     parent      = button.parent(),
                     balance     = vip.selector.sellForm.find('#tab1-jual span[class^="balance_"]').text().replaceAll('.', ''),
                     marketPrice = vip.selector.spotStat.find('.market-price-box > strong').text().replaceAll('.', ''),
                     clPrice     = vip.selector.spotStat.find('.trading.cut-loss > strong').text().replaceAll(',', ''),
                     sellPrice   = 0;

                 balance = base.cancelOrder( balance, 'sell');

                 parent.find('.sell-button').removeClass('active');
                 button.addClass('active');

                 if ( button.hasClass('sell-cut-loss') )
                 {
                     sellPrice = parseInt(clPrice);
                 }
                 else if ( button.hasClass('sell-three') )
                 {
                     sellPrice = base.calculatePricePercent(marketPrice, 3, '+');
                 }
                 else if ( button.hasClass('sell-six') )
                 {
                     sellPrice = base.calculatePricePercent(marketPrice, 6, '+');
                 }
                 else if ( button.hasClass('sell-last-price') )
                 {
                     sellPrice = base.calculatePriceDigit(marketPrice, '+');
                 }
                 else if ( button.hasClass('sell-panic') )
                 {
                     sellPrice = parseInt(marketPrice) / 2;
                 }

                 vip.selector.sellForm.find('#tab1-jual .input-group > input[id^="sell_amount_"]').val(balance).change();
                 vip.selector.sellForm.find('#sell_price').val(sellPrice).change();

                 base.processTrade(vip.selector.sellForm);
             });
         },
         tradeBuy : function()
         {
             var base = this;

             vip.selector.body.on('click', '.buy-button', function(e)
             {
                 e.preventDefault();

                 var button      = $(this),
                     parent      = button.parent(),
                     balanceIDR  = vip.selector.buyForm.find('#tab1-beli .balance_rp_val').text().replaceAll('.', ''),
                     marketPrice = vip.selector.spotStat.find('.market-price-box > strong').text().replaceAll('.', ''),
                     buyPrice    = 0;

                 balanceIDR = base.cancelOrder( balanceIDR, 'buy');

                 parent.find('.buy-button').removeClass('active');
                 button.addClass('active');

                 if ( button.hasClass('buy-one') )
                 {
                     buyPrice = base.calculatePricePercent(marketPrice, 1, '-');
                 }
                 else if ( button.hasClass('buy-two') )
                 {
                     buyPrice = base.calculatePricePercent(marketPrice, 2, '-');
                 }
                 else if ( button.hasClass('buy-three') )
                 {
                     buyPrice = base.calculatePricePercent(marketPrice, 3, '-');
                 }
                 else if ( button.hasClass('buy-last-price') )
                 {
                     buyPrice = base.calculatePriceDigit(marketPrice, '-');
                 }
                 else if ( button.hasClass('buy-panic') )
                 {
                     buyPrice = parseInt(marketPrice) * 2;
                 }

                 vip.selector.buyForm.find('#tab1-beli .input-group > input[id^="buy_amount_"]').val(balanceIDR).change();
                 vip.selector.buyForm.find('#buy_price').val(buyPrice).change();

                 base.processTrade(vip.selector.buyForm);
             });
         },
         processTrade : function(element)
         {
             if ( vip.constant.DEVMODE ) return;

             setTimeout( function()
             {
                 element.find('.wizard .next').trigger('click');
                 element.find('.wizard .next').trigger('click');
             }, 1000);
         },
         cancelOrder : function( balance, status)
         {
             var base            = this,
                 tabel           = vip.selector.cancelOrder.find('table'),
                 cancelBalance   = 0;

             if ( tabel.length )
             {
                 tabel.find('tbody tr').each(function()
                 {
                     var element      = $(this).find('td').last(),
                         regExp       = /cancel_order\(([^)]+)\)/,
                         matches      = regExp.exec(element.find('a').attr('onclick')),
                         orderID      = matches[1].split(' ')[1],
                         orderBalance = $($(this).find('td').get(4)).text().split(' ')[0].replaceAll('.', '');

                         cancelBalance = cancelBalance + parseInt(orderBalance);

                         cancel_order(status, parseInt(orderID));
                 });
             }

             if ( cancelBalance > 0 )
             {
                 balance = cancelBalance;
             }

             return balance;
         },
         calculatePriceDigit : function(price, operator)
         {
             if ( price.length >= 6 )
             {
                 if ( operator === '+' )
                 {
                     price = parseInt(price) + 100;
                 } else {
                     price = parseInt(price) - 100;
                 }
             } else if ( price.length >= 5 ) {
                 if ( operator === '+' )
                 {
                     price = parseInt(price) + 10;
                 } else {
                     price = parseInt(price) - 10;
                 }
             } else {
                 if ( operator === '+' )
                 {
                     price = parseInt(price) + 2;
                 } else {
                     price = parseInt(price) - 2;
                 }
             }

             return price;
         },
         calculatePricePercent : function(price, percent, operator)
         {
             price   = parseInt(price);
             percent = (price * percent) / 100;

             if ( operator === '+' )
             {
                 price   = price + parseInt(percent);
             } else {
                 price   = price - parseInt(percent);
             }

             return price;
         },
         tradeSignal : function()
         {
             var base = this;

             setInterval(function()
             {
                 $(vip.constant.CURRENCY).each(function(index, item)
                 {
                     base.tradeSignalCheck(item);
                 });
             }, vip.constant.SIGNAL_INTERVAL);
         },
         tradeSignalCheck : function(currency)
         {
             var base      = this,
                 index     = 0,
                 percent   = 3, // use 3% margin
                 maxLength = 60, // only check last 30 minutes price record
                 price, prices, percentPrice;

             prices  = vip.data.getPriceRecord(currency);

             price   = vip.selector.markets.find('tbody span[class="price_' + currency + 'idr_val"]').text();
             price   = parseInt(price.replaceAll('.', ''));

             percent = (price * percent) / 100;

             percentPrice = price - percent;

             for ( var i = prices.length - 1; i >= 0; i-- )
             {
                 if( prices[i][1] <= percentPrice || index === maxLength )
                 {
                     index = index / 2;

                     // formula = deteksi peningkatan 3% dalam 5 - 10 menit
                     // note : semakin kecil index = semakin besar sinyal beli (pump)

                     if ( index <= 10 )
                     {
                         // console.log('pumping ' + currency, prices.length, prices[i][1], percentPrice, index + ' menit');
                         base.doSignalRecord(currency, [$.now(), price, percentPrice, index + ' menit']);
                     }
                     break;
                 }

                 index++;
             }
         },
         doSignalRecord : function(currency, data)
         {
             var base = this,
                 signal;

             signal = base.getSignalRecord(currency);
             signal.push(data);

             localStorage.setItem(base.getSignalRecordKey(currency), JSON.stringify(signal));
         },
         getSignalRecord : function(currency)
         {
             var base   = this,
                 signal = localStorage[base.getSignalRecordKey(currency)];

             if ( typeof signal !== 'undefined' )
             {
                 return JSON.parse(signal);
             }

             return [];
         },
         getSignalRecordKey : function(currency)
         {
             var base = this;

             return base.signalRecordPrefix + currency;
         },
         resetSignalRecord : function(currency)
         {
             var base = this;

             if ( currency === 'all' )
             {
                 $(vip.constant.CURRENCY).each(function(index, item)
                 {
                     localStorage.removeItem(base.getSignalRecordKey(item));
                 });
             } else {
                 localStorage.removeItem(base.getSignalRecordKey(currency));
             }
         },
         tradeRecord : function(currency, data)
         {
             var base = this,
                 record;

             record = base.getTradeRecord(currency);
             record.push(data);

             localStorage.setItem(base.getTradeRecordKey(currency), JSON.stringify(record));
         },
         getTradeRecord : function(currency)
         {
             var base   = this,
                 record = localStorage[base.getTradeRecordKey(currency)];

             if ( typeof record !== 'undefined' )
             {
                 return JSON.parse(record);
             }

             return [];
         },
         getTradeRecordKey : function(currency)
         {
             var base = this;

             return base.tradeRecordPrefix + currency;
         },
         resetTradeRecord : function(currency)
         {
             var base = this;

             if ( currency === 'all' )
             {
                 $(vip.constant.CURRENCY).each(function(index, item)
                 {
                     localStorage.removeItem(base.getTradeRecordKey(item));
                 });
             } else {
                 localStorage.removeItem(base.getTradeRecordKey(currency));
             }
         },
    };

    vip.trade.init();



    /**********************
     * Trading Logic
     **********************/

    window.vip.tradeLogic =
    {
        // Trading Data
        tradingStatus               : vip.constant.TRADE_SELL,
        buyPrice                    : null,
        profitStage                 : null,
        tradingBegin                : null,
        tradeData                   : [],
        priceDropTolerance          : 2,
        priceDropToleranceCount     : 0,
        priceBounceBackTolerance    : 0,

        init: function()
        {
            this.buyLogic();
            this.sellLogic();
        },

        checkTradeStatus : function()
        {
            return this.tradingStatus
        },

        setTradeStatus : function(status){
            this.tradingStatus = status;
        },

        buyLogic : function()
        {
            var base = this;

            $(window).bind('price-statistic', function()
            {
                if(base.checkTradeStatus() === vip.constant.TRADE_SELL)
                {
                    base.buyDecision();
                }
            });
        },

        sellLogic: function()
        {
            var base = this;
            var price = 0;

            $(window).bind('price-statistic', function()
            {
                if(base.checkTradeStatus() === vip.constant.TRADE_BUY)
                {
                    if(price = vip.helper.getLastPrice())
                    {
                        // base.sellDecision1(price);
                        base.sellDecision2(price);
                    }
                }
            });
        },

        /**
         * Sell Logic v1
         *
         * @param price (latest price)
         * @todo :
         *  1. Hold Mechanism pake percentage saja. misal nya turun 0.5% masih hold, kalau sudah dibawah 0.5 langsung sell
         *  2. Harus nya baru mulai trading, sudah -0.6 karena ada biaya trading 0.3 setiap transaksi buy & sell
         *  3. Nilai cut lost di sesuaikan dengan biaya trading (mungkin -1.0)
         *  4. Jadi ga usah pake trading hold tolerance sama profit stage
         */
        sellDecision1: function(price)
        {
            var base = this;
            var diff = price - base.buyPrice;
            var diffPercentage = diff / base.buyPrice * 100;
            var diffStage = diffPercentage.toFixed(0);

            console.log("Price Progress " + diffPercentage.toFixed(3));

            if(diffPercentage < vip.constant.CUT_LOSS_PERCENTAGE)
            {
                base.tradeSell(price, diffPercentage.toFixed(3));
            } else {

                // profit stage
                if(base.profitStage === diffStage)
                {
                    // Reset Price Drop Tolerance
                    base.priceDropToleranceCount = base.priceDropTolerance;
                } else if (base.profitStage < diffStage)
                {
                    // hold
                    base.profitStage = diffStage;
                } else if(base.profitStage > diffStage)
                {
                    // @todo : check if we can hold it further before we actually selling
                    if(base.priceDropToleranceCount === 0)
                    {
                        base.tradeSell(price, diffPercentage.toFixed(3));
                    } else {
                        base.priceDropToleranceCount--;
                        console.log('Prevent Selling, tolerance remaining : ' + base.priceDropToleranceCount);
                    }
                }
            }
        },


        /**
         * Sell Logic v2
         *
         * @param price (latest price)
         * @todo :
         *  1. buy price + 0.6 tidak bisa dipakai karena diffPercentage pertama bisa langsung jadi -0.6, atau kurangi CUT_LOSS_PERCENTAGE - 0.6
         *  2. priceDropTolerance harus di batasi beberapa kali juga misalnya cuma 2 kali
         */
        sellDecision2: function(price)
        {
            var base                = this;
            var diff                = price - base.buyPrice;
            var diffPercentage      = diff / base.buyPrice * 100;
            var priceDropToleranceGap = 0;

            console.log("Price Progress " + diffPercentage.toFixed(3) + "% | " + vip.helper.formatPrice(price));

            if (diffPercentage < vip.constant.CUT_LOSS_PERCENTAGE)
            {
                base.tradeSell(price, diffPercentage.toFixed(3));
            } else {
                //profit stage
                if (base.profitStage === diffPercentage)
                {
                    // hold
                } else if (diffPercentage > base.profitStage)
                {
                    // hold
                    base.priceBounceBackTolerance = base.profitStage;
                    base.profitStage = diffPercentage;
                } else if (diffPercentage < base.profitStage)
                {
                    priceDropToleranceGap = base.profitStage - diffPercentage;
                    base.priceBounceBackTolerance = (base.priceBounceBackTolerance < diffPercentage ) ? diffPercentage : base.priceBounceBackTolerance;

                    if(vip.constant.PRICE_DROP_TOLERANCE >= priceDropToleranceGap.toFixed(2))
                    {
                        console.log('Prevent selling down to : -' + priceDropToleranceGap.toFixed(2) + '%, Top : ' + base.profitStage.toFixed(2) + '%');
                    } else if (base.priceBounceBackTolerance <= diffPercentage)
                    {
                        console.log('Price bounce back to : ' + diffPercentage.toFixed(2) + '%, Top : ' + base.profitStage.toFixed(2) + '%');
                    } else {
                        base.tradeSell(price, diffPercentage.toFixed(3));
                    }
                }
            }
        },


        /**
         * Check price statistic, if number of row are green than Execute do Buy Trading
         * @todo : cari parameter lain untuk memastikan kalau market sedang bergairah untuk buy
         */
        buyDecision: function()
        {
            var base = this;
            var section = $('.price-statistic').find('tbody tr');
            var buyFlag = true;
            var counter = 0;

            for(var i = 0; i < vip.constant.PRICE_ROW_CHECK; i++)
            {
                $(section.get(i)).find('td > span').each(function()
                {
                    if($(this).hasClass('down'))
                    {
                        buyFlag = false;
                    }
                    counter++;
                });
            }

            if(buyFlag && counter === vip.constant.PRICE_ROW_CHECK * vip.constant.PRICE_STATISTIC)
            {
                base.tradeBuy();
            }
        },

        tradeSell: function(price, percentage)
        {
            var base = this;

            console.log(
                "Sell at : " + price +
                "\nPercentage : " + percentage +
                "\nTime On Market : " + vip.helper.calculateMinutes(base.tradingBegin, vip.helper.getCurrentTime()) + " Minutes"
            );

            base.setTradeStatus(vip.constant.TRADE_SELL);
        },

        /**
         * Trade Log
         * @todo : baru mulai buy price nya ditambah 0.6
         */
        tradeBuy: function()
        {
            var base = this;
            var realPrice = vip.helper.getLastPrice();

            base.buyPrice = realPrice + ( realPrice * vip.constant.MARKET_COST / 100 );
            base.tradingBegin = vip.helper.getCurrentTime();

            // Normalize Data
            base.priceBounceBackTolerance = base.profitStage = vip.constant.MARKET_COST * -1;
            base.priceDropToleranceCount = base.priceDropTolerance;
            base.tradeData = [];

            // Trade Logic
            var target1 = base.buyPrice + (base.buyPrice * 1 / 100);
            var target2 = base.buyPrice + (base.buyPrice * 2 / 100);
            var target3 = base.buyPrice + (base.buyPrice * 3 / 100);
            var cl = base.buyPrice - (base.buyPrice * 1 / 100);

            console.log("Real Price : " + vip.helper.formatPrice(realPrice) +
                "\nBuy at Price : " + vip.helper.formatPrice(base.buyPrice) +
                "\nCut Loss : " + vip.helper.formatPrice(cl) +
                "\ntarget I : " + vip.helper.formatPrice(target1) +
                "\ntarget II : " + vip.helper.formatPrice(target2) +
                "\ntarget III : " + vip.helper.formatPrice(target3) +
                "\nTime Trade  : " + vip.helper.getCurrentTimeString()
            );

            base.setTradeStatus(vip.constant.TRADE_BUY);

            return "..::Trade Starting::..";
        }

    };

    vip.tradeLogic.init();



})(jQuery);